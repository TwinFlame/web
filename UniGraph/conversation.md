Hi @pond#6519, @Black Swan Harveztor#8195 I'm here. Sorry, I'm late!

_"what infrastructure are you using for the foundry? I mean which language / framework are you building on?"_

We're just using an off the shelf NextCloud – With some **theming customizations** by Andreas @xirtus#3165.  All of the features you see are "Community Apps"! They're just plain Php & SQL, and running on shared hosting.  https://apps.nextcloud.com  You get 3 database choices when you install. If you've tried some of the advanced features? Like Video chat? That's `WebSockets` based.

We're using 2 versions behind the master though, because `latest` was EXTREMELY unstable! We connected a Matomo instance to it to record stats. http://stat.tartarynova.com NextCloud is actually a fork from a much older project: The lead developers were fed up, it was too commercial & restrictive, and created NextCloud. :wink: https://en.wikipedia.org/wiki/Nextcloud#History_of_the_fork_from_ownCloud 

_"I see, but... how will you manage the data?"_

Right now it's all manual, uploaded via browser, and a pita!  You can though connect a phone or desktop clients: https://nextcloud.com/clients Also - we're going to have some major improvements soon, by connecting to Minio Object Store & possibly WebTorrent or IPFS for Video. And we need a simple local backup & Sync tool, based on`minio mc`.

I Should mention, with a good VPS or Cloud instance you can extend Foundry. much beyond what you see here - Incl. Rich Text Documents, like Google Docs. _But we chose to work within the confines of Shared hosting for now, to reduce cost_

—If you need help setting up something similar? PM me I can walk you through. You only need a Shared hosting, or a lowendbox.com.
Or If you have a decent destkop/laptop? I Can get you a Foundry running locally on Vagrant.